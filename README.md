# springboot-cloud-config-server
### based on

- https://cloud.spring.io/spring-cloud-static/spring-cloud-config/2.0.1.RELEASE/single/spring-cloud-config.html
- https://cloud.spring.io/spring-cloud-static/spring-cloud.html
- https://o7planning.org/en/11723/understanding-spring-cloud-config-server-with-example
- http://localhost:8888/some-application/application.properties

### About Configuration
- configuration file should be "bootstrap" because it has hig precedence over .properties file
- bootstrap.properties can't resolve placeholders like {application} or {profile}
- client application should include application name and active profiles properties to browse in repo folder structure
- if you need read from particular branch, you should use default-label property
- searchPaths property use values between ''
- you can define more than one path in searchPaths property using comma separator

